#include "confighelper.h"

ConfigHelper::ConfigHelper(QString path) : m_configPath(path)
{

}

ConfigHelper::~ConfigHelper()
{

}

QVariant ConfigHelper::readFromFile( QString key ) const
{
    QSettings setting(m_configPath,QSettings::IniFormat);
    return setting.value(key);
}
