#ifndef WEBENGINEVIEW_H
#define WEBENGINEVIEW_H

#include <QWebEngineView>
#include <QDesktopServices>

class MainWindow;

class WebEngineView : public QWebEngineView
{
    Q_OBJECT
public:
    WebEngineView(QWidget* parent = nullptr);

protected:
    QWebEngineView * createWindow(QWebEnginePage::WebWindowType type) override;

private:
    MainWindow *m_pMainWindow{nullptr};
};

#endif // WEBENGINEVIEW_H
