#include "webengineview.h"
#include "mainwindow.h"

WebEngineView::WebEngineView(QWidget *parent)
    : QWebEngineView(parent)
{

}

QWebEngineView *WebEngineView::createWindow(QWebEnginePage::WebWindowType type)
{
    Q_UNUSED(type)
    this->m_pMainWindow = new MainWindow(this);
    this->m_pMainWindow->show();
    return this->m_pMainWindow->createView();
}

