#ifndef CONFIGHELPER_H
#define CONFIGHELPER_H

#include <QSettings>

class ConfigHelper
{
public:
    ConfigHelper( QString path );
    ~ConfigHelper();

    template<typename T>
    void writeToFile( QString key, T&& value ) const;

    QVariant readFromFile( QString key ) const;

private:
    QString m_configPath{""};
};

template<typename T>
void ConfigHelper::writeToFile(QString key, T&& value ) const
{
    QSettings setting(m_configPath,QSettings::IniFormat);
    setting.setValue(key, value);
}

#endif // CONFIGHELPER_H
