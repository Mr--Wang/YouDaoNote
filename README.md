# YouDaoNote
Linux下的有道云笔记封装，使用Qt Creator IDE进行开发。


# 待开发功能
- 字体更换
- 添加返回，前进按钮
- 将Widget修改为QML实现
- 增加下载进度条

# Bug 列表

- QLayout: Attempting to add QLayout "" to MainWindow "MainWindow", which already has a layout  
- 不能点击有道云笔记内的链接，否则会弹出新的窗口，后面解决为调用系统默认浏览器打开
- 解决鼠标滚轮有反弹的bug
- unity 双击不能显示程序

# 快捷键列表
1. CTRL+R  
重置缩放大小为1
2. CTRL+F
页面关键字查找
3. CTRL+滚轮
页面进行缩放
4. ESC
窗口最小化