#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWebEngineView>
#include <QWebEngineSettings>
#include <QWebEngineProfile>
#include <QWheelEvent>
#include <QVBoxLayout>
#include <QAction>
#include <QMenu>
#include <QSystemTrayIcon>    //用于系统图标
#include <QContextMenuEvent> //用于托盘菜单实现
#include <QUrl>
#include <QIcon>
#include <QMessageBox>
#include <QLineEdit>         //网页查找功能
#include <QToolButton>
#include <chrono>
#include <thread>
#include <QFileDialog>

#include "webengineview.h"
#include "confighelper.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    Q_SLOT void onFullScreen();
    Q_SLOT void onExitFullScreen();
    Q_SLOT void onResetZoom();
    Q_SLOT void onFindText();


    void createActions();
    void createMenu();

    WebEngineView * createView();

protected slots:
    void closeEvent( QCloseEvent * event );
    void on_showMainAction();
    void on_exitAppAction();
    void on_activatedSysTrayIcon(QSystemTrayIcon::ActivationReason reason);
    void resizeEvent(QResizeEvent *);
    void wheelEvent(QWheelEvent *event);
    void clickButton();
    void downloadRequested(QWebEngineDownloadItem* download);

private:
    int m_flag{0};
    bool m_isHide{false};
    double m_zoomScale{1.0};
    QString m_configPath{"/opt/ydnote/setting.ini"};
    QString m_zoomKey{"zoomScale"};

    QString m_site{"https://note.youdao.com/"
                   "signIn/"
                   "index.html?&callback=https%3A%2F%2Fnote.youdao.com%2Fweb%2F&from=web"};

    Ui::MainWindow *m_pUi;
    WebEngineView *m_pView{nullptr};
    QSystemTrayIcon *m_pSysTrayIcon{nullptr};
    QMenu *m_pMenu{nullptr};
    QVBoxLayout *m_pLayout{nullptr};

    QAction *m_pShowMainAction{nullptr};
    QAction *m_pExitAppAction{nullptr};
    QAction *m_pEscAct;
    QAction *m_pResetZoom{nullptr};
    QAction *m_pLookup{nullptr};

    QLineEdit *m_pFindEdit{nullptr};
    QToolButton *m_pButton{nullptr};

};

#endif // MAINWINDOW_H
